/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Charathram Ranganathan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

package org.calpilot;



/* Pi4J imports */

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

/**
 * Created by calpilot on 5/13/2014.
 */
public class Tutorial1 {

	public static void main(String[] args) {
		System.out.println("Hello, Raspberry Pi!");

        /* Initialize pi4j */
		final GpioController gpio = GpioFactory.getInstance();

        /* Initialize GPIO 0 as an input pin called "MyButton" and set
        it low using the pull-down resistor.
        */
		GpioPinDigitalInput myButton =
				gpio.provisionDigitalInputPin(RaspiPin.GPIO_00,
						"MyButton",
						PinPullResistance.PULL_DOWN);

        /* Read the state (high or low) of the pin. Remember, it should be "low" */
		PinState pinState = myButton.getState();
		System.out.println("GPIO 0 is set to " + pinState.getName());

        /* Close all open connections. */
		gpio.shutdown();
	}
}